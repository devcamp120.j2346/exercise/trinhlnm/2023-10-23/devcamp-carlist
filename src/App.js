import {cars, checkNew} from "./info";

function App() {
  return (
    <div>
      <ul>
        {cars.map((e, index) => {
          return <li key={index}>Ô tô {e.make}, biển số {e.vID}, {checkNew(cars[index].year)}</li>;
        })}
      </ul>
    </div>
  );
}

export default App;
